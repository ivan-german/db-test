INSERT INTO clients (name) VALUES
  ('Chuck Norris'),
  ('Nil Armstrong');

DELETE FROM clients
  WHERE id IN (5, 6);

UPDATE clients
  SET name = 'Alexandr Mikhalchuk'
  WHERE id = 1;

INSERT INTO clients (name)
  SELECT name FROM authors;

SELECT b.title AS authors
  FROM books AS b
  JOIN book_author AS ba ON ba.book_id = b.id
  JOIN authors AS a ON ba.author_id = a.id
GROUP BY b.title
ORDER BY title DESC;

UPDATE books
SET owner_id = 3
WHERE id = 3;
