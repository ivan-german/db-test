-- СОЗДАНИЕ ТАБЛИЦ

CREATE TABLE authors (
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(128)
);

CREATE TABLE books (
    id BIGINT PRIMARY KEY NOT NULL,
    title VARCHAR(128),
    owner_id BIGINT,
    FOREIGN KEY (owner_id) REFERENCES CLIENTS (id)
);

CREATE TABLE book_author (
    book_id BIGINT,
    author_id BIGINT,
    FOREIGN KEY (author_id) REFERENCES AUTHORS (id),
    FOREIGN KEY (book_id) REFERENCES BOOKS (id)
);

CREATE TABLE clients (
    id BIGINT PRIMARY KEY NOT NULL,
    name VARCHAR(128)
);



-- ВСТАВКА ДАННЫХ

INSERT INTO clients(name) VALUES
  ('Alexander Mikhalchuk'),
  ('Ivan German');

INSERT INTO books(title, owner_id) VALUES
  ('Effective Java 2nd edition', 2),
  ('Thinking in Java 4th edition', 2),
  ('Javascript for dummies', NULL);

INSERT INTO authors(name) VALUES
  ('Bruce Eckel'),
  ('Joshua Bloch');

INSERT INTO book_author VALUES
  (1, 2),
  (2, 1),
  (3, 1),
  (3, 2);
